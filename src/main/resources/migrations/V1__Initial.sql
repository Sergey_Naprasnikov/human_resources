CREATE SEQUENCE hibernate_sequence;


CREATE TABLE groups (
  id                  SERIAL PRIMARY KEY,
  name                VARCHAR
);

CREATE TABLE students (
  id                  SERIAL PRIMARY KEY,
  name                VARCHAR,
  surname             VARCHAR,
  patronymic          VARCHAR,
  year_birth          DATE,
  year_revenue        DATE,
  group_id            BIGINT,
  speciality_id BIGINT,
  phone_student VARCHAR,
  phone_parents VARCHAR,
  address VARCHAR,
  sex_id BIGINT DEFAULT 3,
  passport_data VARCHAR,
  indifikatsionny_code VARCHAR,
  form_training_id BIGINT,
  year_issue DATE,
  location VARCHAR,
  atestat VARCHAR,
  certificate VARCHAR,

  type_user           VARCHAR
);

CREATE TABLE sex (
  id                  SERIAL PRIMARY KEY,
  name                VARCHAR
);

CREATE TABLE form_training (
  id                  SERIAL PRIMARY KEY,
  name                VARCHAR
);

INSERT INTO sex(name) VALUES ('Мужской');
INSERT INTO sex(name) VALUES ('Женский');
INSERT INTO sex(name) VALUES ('Не определено');

INSERT INTO form_training(name) VALUES ('Бюджет');
INSERT INTO form_training(name) VALUES ('Контракт');
INSERT INTO form_training(name) VALUES ('Не определено');

CREATE TABLE employees (
  id                  SERIAL PRIMARY KEY,

  name                VARCHAR,
  surname             VARCHAR,
  patronymic          VARCHAR,
  date_of_birth       TIMESTAMP DEFAULT now(),
  position_id         BIGINT,
  date_of_issue       TIMESTAMP DEFAULT now(),
  phone               VARCHAR,
  home_address        VARCHAR,
  place_of_residence  VARCHAR,
  series_passport_number VARCHAR,
  indifikatsionny_code VARCHAR,
  type_user           VARCHAR
);

CREATE TABLE position (
  id                  SERIAL PRIMARY KEY,

  position_name        VARCHAR

);

CREATE TABLE speciality (
  id                  SERIAL PRIMARY KEY,

  name_speciality     VARCHAR
);

CREATE TABLE history_reports (
  id                  SERIAL PRIMARY KEY,
  report_name_id      VARCHAR NOT NULL,
  name                VARCHAR,
  surname             VARCHAR,
  patronymic          VARCHAR,
  date                TIMESTAMP,
  type_user           VARCHAR
);

CREATE TABLE report_names (
  id                  SERIAL PRIMARY KEY,
  name                VARCHAR,
  report_type         VARCHAR

);

INSERT INTO report_names(name, report_type) VALUES ('Справка военкомату', 'ECRUITMENT_OFFICE');