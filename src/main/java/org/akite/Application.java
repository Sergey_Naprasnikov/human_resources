package org.akite;

import javafx.scene.Scene;
import javafx.stage.Stage;
import org.akite.ui.view.MainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "org.akite.repository")
public class Application extends AbstractJavaFxApplicationSupport{

    @Value("${ui.title:Планировщик}")//
    private String windowTitle;

    @Autowired
    private MainView view;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(view.getView()));
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.show();
    }

    public static void main(String[] args) {
        launchApp(Application.class, args);
    }

}
