package org.akite.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;

@Configuration
public class Config {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Bean(name = "importStudents")
    public SimpleJdbcInsert getImportStudents() {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert.setTableName("students");
        simpleJdbcInsert.setGeneratedKeyName("id");
        return simpleJdbcInsert;
    }

}
