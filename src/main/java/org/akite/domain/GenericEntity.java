package org.akite.domain;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Component
@MappedSuperclass
public class GenericEntity{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    protected void setId(final Long id) {
        this.id = id;
    }
}
