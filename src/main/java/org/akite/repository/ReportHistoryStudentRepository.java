package org.akite.repository;

import org.akite.domain.ReportHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Admin on 09.04.2016.
 */
@Repository
public interface ReportHistoryStudentRepository extends CrudRepository<ReportHistory, Long> {
}
