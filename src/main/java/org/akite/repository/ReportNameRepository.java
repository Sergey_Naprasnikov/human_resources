package org.akite.repository;

import org.akite.domain.ReportName;
import org.akite.domain.ReportType;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Admin on 09.04.2016.
 */
public interface ReportNameRepository extends CrudRepository<ReportName, Long> {

    ReportName findByReportType(ReportType reportType);

}
