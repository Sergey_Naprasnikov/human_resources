package org.akite.ui.element;

import javafx.scene.control.TextField;

/**
 * Created by Серёга on 29.04.2016.
 */
public class NumberField extends TextField {
    @Override
    public void replaceText(int start, int end, String text) {
        if (text.matches("[0-9]*")){
            super.replaceText(start, end, text);
        }
    }
    @Override
    public void replaceSelection (String text) {
        if (text.matches("[0-9]*")){
            super.replaceSelection(text);
        }
    }
}
