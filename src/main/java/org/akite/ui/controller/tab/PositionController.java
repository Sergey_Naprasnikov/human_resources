package org.akite.ui.controller.tab;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.akite.domain.Position;
import org.akite.service.PositionService;
import org.akite.ui.dialog.DialogWindow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Admin on 18.03.2016.
 */
@Component
public class PositionController {

    @Autowired
    PositionService service;
    @FXML
    TableView<Position> positionTable;

    @FXML
    public void initialize() {
        initElement();
        initMenu();
        initData();
    }

    @FXML
    public void onAdd() {
        TextField nameTextField = new TextField();
        DialogWindow window = createAddDialog("", "Добавить", nameTextField);

        window.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Position position = new Position();
                position.setName(nameTextField.getText());
                positionTable.getItems().add(position);
                service.add(position);
                window.close();
            }
        });
        window.show();
    }

    @FXML
    public void onEdit() {
        Position position =  positionTable.getSelectionModel().getSelectedItem();

        if (position != null) {
            TextField nameTextField = new TextField();
            DialogWindow window = createAddDialog("", "Редактировать", nameTextField);

            nameTextField.setText(position.getName());

            window.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    position.setName(nameTextField.getText());
                    positionTable.getItems().set(positionTable.getSelectionModel().getSelectedIndex(), position);
                    service.update(position);
                    window.close();
                }
            });
            window.show();
        }
    }

    @FXML
    public void onDelete() {
        Position position =  positionTable.getSelectionModel().getSelectedItem();
        if(position != null) {
            DialogWindow dialogWindow = new DialogWindow();
            dialogWindow.setTitle("Удаление");
            dialogWindow.getPanel().getChildren().addAll(new Label("Удалить ?"), dialogWindow.getButtons());
            dialogWindow.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    service.delete(position);
                    positionTable.getItems().remove(position);
                    dialogWindow.close();
                }
            });

            dialogWindow.show();
        }
    }

    private DialogWindow createAddDialog(String value, String titleWindow, TextField textField) {
        DialogWindow dialogWindow = new DialogWindow();
        dialogWindow.setTitle(titleWindow);
        textField.setText(value);
        textField.setPromptText("Имя");
        dialogWindow.getPanel().getChildren().addAll(textField, dialogWindow.getButtons());
        return dialogWindow;
    }



    private void initData() {
        positionTable.setItems(FXCollections.observableArrayList(service.getAll()));
    }

    private void initElement() {
        TableColumn name = new TableColumn("Должность");
        name.setPrefWidth(181.0);
        name.setCellValueFactory(
                new PropertyValueFactory<Position, String>("name"));

        positionTable.getColumns().add(name);
    }

    private void initMenu() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem add = new MenuItem("Добавить");
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onAdd();
            }
        });
        contextMenu.getItems().add(add);

        MenuItem edit = new MenuItem("Редактировать");
        edit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onEdit();
            }
        });
        contextMenu.getItems().add(edit);

        MenuItem delete = new MenuItem("Удалить");
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onDelete();
            }
        });
        contextMenu.getItems().add(delete);

        positionTable.setContextMenu(contextMenu);
    }

}
