package org.akite.ui.controller.tab;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.akite.domain.*;
import org.akite.service.*;
import org.akite.ui.dialog.DialogWindow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by Admin on 29.01.2016.
 */
@Component
public class StudentsController {

    @Autowired
    StudentService studentService;
    @Autowired
    GroupService groupService;
    @Autowired
    SpecialityService specialityService;
    @Autowired
    SexService sexService;
    @Autowired
    FormTrainingService formTrainingService;
    @Autowired
    ExportService exportService;

    @FXML
    TableView<Student> studentsTable;

    @FXML
    public void initialize() {
        initElement();
        initData();
        initMenu();
    }

    private void initData() {
        studentsTable.setItems(FXCollections.observableArrayList(studentService.getAll()));
    }

    private DialogWindow createDialog(String title,TextField nameField, TextField surnameField,TextField patronymicField,
                                      DatePicker yearRevenuePicker, DatePicker yearBirthPicker, ComboBox<Group> groupComboBox,
                                      ComboBox<Speciality> specialityComboBox, TextField phoneStudentField,
                                      TextField phoneParentsField,TextField addressField,  ComboBox<Sex> sexComboBox,
                                      TextField passportDataField, TextField indifikatsionnyCode,  ComboBox<FormTraining> formTrainingComboBox) {


        DialogWindow dialogWindow = new DialogWindow();
        dialogWindow.setTitle(title);
        dialogWindow.setHeight(600);

        nameField.setPromptText("Имя Студента");
        surnameField.setPromptText("Фамилия Студента");
        patronymicField.setPromptText("Отчество Студента");
        phoneStudentField.setPromptText("Телефон Студента");
        phoneParentsField.setPromptText("Телефон Родителей");
        addressField.setPromptText("Адрес");
        passportDataField.setPromptText("Паспортные данные");
        indifikatsionnyCode.setPromptText("Идентификационный код");

        yearRevenuePicker.setValue(LocalDate.now());
        yearBirthPicker.setValue(LocalDate.now());
        groupComboBox.setItems(FXCollections.observableArrayList(groupService.getAll()));

        groupComboBox.getSelectionModel().selectFirst();
        groupComboBox.setPromptText(groupComboBox.getSelectionModel().getSelectedItem().getName());

        groupComboBox.setCellFactory(new Callback<ListView<Group>, ListCell<Group>>() {
            @Override
            public ListCell<Group> call(ListView<Group> param) {

                return new ListCell<Group>() {
                    @Override
                    public void updateItem(Group item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            setText(item.getName());
                            setGraphic(null);
                            groupComboBox.setPromptText(groupComboBox.getSelectionModel().getSelectedItem().getName());
                        } else {
                            setText(null);
                            groupComboBox.setPromptText(groupComboBox.getSelectionModel().getSelectedItem().getName());
                        }
                    }
                };
            }
        });


        specialityComboBox.setItems(FXCollections.observableArrayList(specialityService.getAll()));

        specialityComboBox.getSelectionModel().selectFirst();

        specialityComboBox.setCellFactory(new Callback<ListView<Speciality>, ListCell<Speciality>>() {
            @Override
            public ListCell<Speciality> call(ListView<Speciality> param) {

                return new ListCell<Speciality>() {
                    @Override
                    public void updateItem(Speciality item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            setText(item.getName());
                            setGraphic(null);
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });

        sexComboBox.setItems(FXCollections.observableArrayList(sexService.getAll()));

        sexComboBox.getSelectionModel().selectFirst();

        sexComboBox.setCellFactory(new Callback<ListView<Sex>, ListCell<Sex>>() {
            @Override
            public ListCell<Sex> call(ListView<Sex> param) {

                return new ListCell<Sex>() {
                    @Override
                    public void updateItem(Sex item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            setText(item.getName());
                            setGraphic(null);
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });

        formTrainingComboBox.setItems(FXCollections.observableArrayList(formTrainingService.getAll()));

        formTrainingComboBox.getSelectionModel().selectFirst();

        formTrainingComboBox.setCellFactory(new Callback<ListView<FormTraining>, ListCell<FormTraining>>() {
            @Override
            public ListCell<FormTraining> call(ListView<FormTraining> param) {

                return new ListCell<FormTraining>() {
                    @Override
                    public void updateItem(FormTraining item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            setText(item.getName());
                            setGraphic(null);
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });

        dialogWindow.getPanel().getChildren().addAll(new Label("Имя Студента"), nameField);
        dialogWindow.getPanel().getChildren().addAll(new Label("Фамилия Студента"), surnameField);
        dialogWindow.getPanel().getChildren().addAll(new Label("Отчество Студента"), patronymicField);
        dialogWindow.getPanel().getChildren().addAll(new Label("Дата поступления"), yearRevenuePicker);
        dialogWindow.getPanel().getChildren().addAll(new Label("Дата Рождения"), yearBirthPicker);
        dialogWindow.getPanel().getChildren().addAll(new Label("Группа"), groupComboBox);
        dialogWindow.getPanel().getChildren().addAll(new Label("Специальность"), specialityComboBox);
        dialogWindow.getPanel().getChildren().addAll(new Label("Телефон Студента"), phoneStudentField);
        dialogWindow.getPanel().getChildren().addAll(new Label("Телефон Родителей"), phoneParentsField);
        dialogWindow.getPanel().getChildren().addAll(new Label("Адрес"), addressField);
        dialogWindow.getPanel().getChildren().addAll(new Label("Пол"), sexComboBox);
        dialogWindow.getPanel().getChildren().addAll(new Label("Паспортные данные"), passportDataField);
        dialogWindow.getPanel().getChildren().addAll(new Label("Идентификационный код"), indifikatsionnyCode);
        dialogWindow.getPanel().getChildren().addAll(new Label("Форма обучения"), formTrainingComboBox);

        dialogWindow.getPanel().getChildren().add(dialogWindow.getButtons());


        return dialogWindow;
    }

    private void initElement() {

        TableColumn nameColumn = new TableColumn("Имя");
        nameColumn.setPrefWidth(181.0);
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("name"));
        studentsTable.getColumns().add(nameColumn);

        TableColumn patronymicColumn = new TableColumn("Отчество");
        patronymicColumn.setPrefWidth(181.0);
        patronymicColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("patronymic"));
        studentsTable.getColumns().add(patronymicColumn);

        TableColumn surnameColumn = new TableColumn("Фамилия");
        surnameColumn.setPrefWidth(181.0);
        surnameColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("surname"));
        studentsTable.getColumns().add(surnameColumn);

        TableColumn yearRevenueColumn = new TableColumn("Дата поступления");
        yearRevenueColumn.setPrefWidth(181.0);
        yearRevenueColumn.setCellValueFactory(
                new PropertyValueFactory<Student, Date>("yearRevenue"));
        studentsTable.getColumns().add(yearRevenueColumn);

        TableColumn yearBirthColumn = new TableColumn("Дата рождения");
        yearBirthColumn.setPrefWidth(181.0);
        yearBirthColumn.setCellValueFactory(
                new PropertyValueFactory<Student, Date>("yearBirth"));
        studentsTable.getColumns().add(yearBirthColumn);

        TableColumn<Student, String> nameGroupColumn = new TableColumn<>("Группа");
        nameGroupColumn.setPrefWidth(100);
        nameGroupColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Student, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getGroup().getName());
            }
        });
        studentsTable.getColumns().add(nameGroupColumn);

        TableColumn<Student, String> specialityColumn = new TableColumn<>("Специальность");
        specialityColumn.setPrefWidth(100);
        specialityColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Student, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getSpeciality().getName());
            }
        });
        studentsTable.getColumns().add(specialityColumn);


        TableColumn phoneStudentColumn = new TableColumn("Телефон");
        phoneStudentColumn.setPrefWidth(181.0);
        phoneStudentColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("phoneStudent"));
        studentsTable.getColumns().add(phoneStudentColumn);

        TableColumn phoneParentsColumn = new TableColumn("Телефон родителей");
        phoneParentsColumn.setPrefWidth(181.0);
        phoneParentsColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("phoneParents"));
        studentsTable.getColumns().add(phoneParentsColumn);

        TableColumn addressColumn = new TableColumn("Адрес");
        addressColumn.setPrefWidth(181.0);
        addressColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("address"));
        studentsTable.getColumns().add(addressColumn);

        TableColumn<Student, String> sexColumn = new TableColumn<>("Пол");
        sexColumn.setPrefWidth(100);
        sexColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Student, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getSex().getName());
            }
        });
        studentsTable.getColumns().add(sexColumn);

        TableColumn passportDataColumn = new TableColumn("Паспортные данные");
        passportDataColumn.setPrefWidth(181.0);
        passportDataColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("passportData"));
        studentsTable.getColumns().add(passportDataColumn);

        TableColumn indifikatsionnyCodeColumn = new TableColumn("Идентификационный код");
        indifikatsionnyCodeColumn.setPrefWidth(181.0);
        indifikatsionnyCodeColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("indifikatsionnyCode"));
        studentsTable.getColumns().add(indifikatsionnyCodeColumn);

        TableColumn<Student, String> formTrainingColumn = new TableColumn<>("Форма обучения");
        formTrainingColumn.setPrefWidth(100);
        formTrainingColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Student, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getFormTraining().getName());
            }
        });
        studentsTable.getColumns().add(formTrainingColumn);

    }


    @FXML
    public void onEdit() {
        Student student = studentsTable.getSelectionModel().getSelectedItem();

        if(student != null) {
            TextField nameField = new TextField();
            TextField surnameField = new TextField();
            TextField patronymicField = new TextField();
            DatePicker yearRevenuePicker = new DatePicker();
            DatePicker yearBirthPicker = new DatePicker();
            ComboBox<Group> groupComboBox = new ComboBox<>();
            ComboBox<Speciality> specialityComboBox = new ComboBox<>();
            TextField phoneStudentField = new TextField();
            TextField phoneParentsField = new TextField();
            TextField addressField = new TextField();
            ComboBox<Sex> sexComboBox = new ComboBox<>();
            TextField passportDataField = new TextField();
            TextField indifikatsionnyCode = new TextField();
            ComboBox<FormTraining> formTrainingComboBox = new ComboBox<>();

            nameField.setText(student.getName());
            surnameField.setText(student.getSurname());
            patronymicField.setText(student.getPatronymic());
            yearRevenuePicker.setValue(student.getYearRevenue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            yearBirthPicker.setValue(student.getYearBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            groupComboBox.setValue(student.getGroup());
            specialityComboBox.setValue(student.getSpeciality());
            phoneStudentField.setText(student.getPhoneStudent());
            phoneParentsField.setText(student.getPhoneParents());
            addressField.setText(student.getAddress());
            sexComboBox.setValue(student.getSex());
            passportDataField.setText(student.getPassportData());
            indifikatsionnyCode.setText(student.getIndifikatsionnyCode());
            formTrainingComboBox.setValue(student.getFormTraining());

            DialogWindow window = createDialog("Редактировать", nameField, surnameField, patronymicField,
                    yearRevenuePicker, yearBirthPicker, groupComboBox, specialityComboBox, phoneStudentField,
                    phoneParentsField, addressField, sexComboBox, passportDataField, indifikatsionnyCode, formTrainingComboBox);

            window.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Student student = new Student();
                    student.setName(nameField.getText());
                    student.setSurname(surnameField.getText());
                    student.setPatronymic(patronymicField.getText());
                    student.setYearRevenue(Date.from(yearRevenuePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                    student.setYearBirth(Date.from(yearBirthPicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                    student.setGroup(groupComboBox.getValue());
                    student.setSpeciality(specialityComboBox.getValue());
                    student.setPhoneStudent(phoneStudentField.getText());
                    student.setPhoneParents(phoneParentsField.getText());
                    student.setAddress(addressField.getText());
                    student.setSex(sexComboBox.getValue());
                    student.setPassportData(passportDataField.getText());
                    student.setIndifikatsionnyCode(indifikatsionnyCode.getText());
                    student.setFormTraining(formTrainingComboBox.getValue());
                    studentService.update(student);
                    studentsTable.getItems().set(studentsTable.getSelectionModel().getSelectedIndex(), student);
                    window.close();
                }
            });

            window.show();
        }

    }

    @FXML
    public void onDelete() {
        Student student =  studentsTable.getSelectionModel().getSelectedItem();
        if(student != null) {
            DialogWindow dialogWindow = new DialogWindow();
            dialogWindow.setTitle("Удаление");
            dialogWindow.getPanel().getChildren().addAll(new Label("Удалить ?"), dialogWindow.getButtons());
            dialogWindow.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    studentService.delete(student);
                    studentsTable.getItems().remove(student);
                    dialogWindow.close();
                }
            });

            dialogWindow.show();
        }
    }

    @FXML
    public void onAdd() {
        TextField nameField = new TextField();
        TextField surnameField = new TextField();
        TextField patronymicField = new TextField();
        DatePicker yearRevenuePicker = new DatePicker();
        DatePicker yearBirthPicker = new DatePicker();
        ComboBox<Group> groupComboBox = new ComboBox<>();
        ComboBox<Speciality> specialityComboBox = new ComboBox<>();
        TextField phoneStudentField = new TextField();
        TextField phoneParentsField = new TextField();
        TextField addressField = new TextField();
        ComboBox<Sex> sexComboBox = new ComboBox<>();
        TextField passportDataField = new TextField();
        TextField indifikatsionnyCode = new TextField();
        ComboBox<FormTraining> formTrainingComboBox = new ComboBox<>();

        DialogWindow window = createDialog("Добавить", nameField,surnameField,patronymicField,
                yearRevenuePicker,yearBirthPicker, groupComboBox,specialityComboBox,phoneStudentField,
                phoneParentsField, addressField,sexComboBox,passportDataField,indifikatsionnyCode,formTrainingComboBox);


        window.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Student student = new Student();
                student.setName(nameField.getText());
                student.setSurname(surnameField.getText());
                student.setPatronymic(patronymicField.getText());
                student.setYearRevenue(Date.from(yearRevenuePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                student.setYearBirth(Date.from(yearBirthPicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                student.setGroup(groupComboBox.getValue());
                student.setSpeciality(specialityComboBox.getValue());
                student.setPhoneStudent(phoneStudentField.getText());
                student.setPhoneParents(phoneParentsField.getText());
                student.setAddress(addressField.getText());
                student.setSex(sexComboBox.getValue());
                student.setPassportData(passportDataField.getText());
                student.setIndifikatsionnyCode(indifikatsionnyCode.getText());
                student.setFormTraining(formTrainingComboBox.getValue());
                studentService.add(student);
                studentsTable.getItems().add(student);
                window.close();
            }
        });

        window.show();

    }


    private void reportEcruitmentOffice() {
        Student student =  studentsTable.getSelectionModel().getSelectedItem();
        if(student != null) {
            Stage stage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Сохранить справку военкомату");
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Doc файл (*.doc)", "*.docx");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showSaveDialog(stage);
            if ((file.getName().equals("") || file.getName() != null)) {
                exportService.exportReportEcruitmentOffice(student, file.getPath());
            }
        }
    }

    private void initMenu() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem add = new MenuItem("Добавить");
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onAdd();
            }
        });
        contextMenu.getItems().add(add);

        MenuItem edit = new MenuItem("Редактировать");
        edit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onEdit();
            }
        });
        contextMenu.getItems().add(edit);

        MenuItem delete = new MenuItem("Удалить");
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onDelete();
            }
        });
        contextMenu.getItems().add(delete);


        Menu reports = new Menu("Отчеты");
        MenuItem reportEcruitmentOffice = new MenuItem("Справка военкомату");
        reports.getItems().add(reportEcruitmentOffice);
        reportEcruitmentOffice.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                reportEcruitmentOffice();
            }
        });


        contextMenu.getItems().add(reports);

        studentsTable.setContextMenu(contextMenu);
    }
}
