package org.akite.ui.controller.tab;

import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import org.akite.domain.Employee;
import org.akite.service.EmployeeService;
import org.akite.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Admin on 18.03.2016.
 */
@Component
public class EmployeesController {

    @FXML
    TableView<Employee> employeesTable;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    PositionService positionService;

    @FXML
    public void initialize() {
        initData();
        initElement();
    }

    private void initData() {
    //    employeesTable.setItems(FXCollections.observableArrayList(employeeService.getAll()));
    }

    private void initElement() {

    }



}
