package org.akite.ui.controller.tab;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import org.akite.domain.ReportHistory;
import org.akite.service.ReportHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.soap.Text;
import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * Created by Admin on 09.04.2016.
 */
@Component
public class HistoryController {

    @FXML
    TableView<ReportHistory> historyTable;
    @Autowired
    ReportHistoryService reportHistoryService;
    @FXML
    TextField filterField;

    private ObservableList<ReportHistory> masterData = FXCollections.observableArrayList();

    @FXML
    public void initialize() {
        initElement();
        initData();
    }

    private void initData() {
        masterData = FXCollections.observableArrayList(reportHistoryService.getAll());

        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<ReportHistory> filteredData = new FilteredList<>(masterData, p -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(reportHistory -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (reportHistory.getName().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true;
                } else {
                    if (reportHistory.getSurname().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                        return true;
                    } else {
                        if (reportHistory.getPatronymic().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                            return true;
                        } else {
                            if (reportHistory.getReportName().getName().indexOf(lowerCaseFilter) != -1) {
                                return true;
                            }
                        }
                    }
                }
                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<ReportHistory> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        // 	  Otherwise, sorting the TableView would have no effect.
        sortedData.comparatorProperty().bind(historyTable.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        historyTable.setItems(sortedData);

    }





    private void initElement() {
        TableColumn<ReportHistory, String> surnameColumn = new TableColumn<>("Фамилия");
        surnameColumn.setPrefWidth(100);
        surnameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReportHistory, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ReportHistory, String> param) {
                return new SimpleStringProperty(param.getValue().getSurname());
            }
        });
        historyTable.getColumns().add(surnameColumn);

        TableColumn<ReportHistory, String> nameColumn = new TableColumn<>("Имя");
        nameColumn.setPrefWidth(100);
        nameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReportHistory, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ReportHistory, String> param) {
                return new SimpleStringProperty(param.getValue().getName());
            }
        });
        historyTable.getColumns().add(nameColumn);

        TableColumn<ReportHistory, String> patronymicColumn = new TableColumn<>("Отчество");
        patronymicColumn.setPrefWidth(100);
        patronymicColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReportHistory, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ReportHistory, String> param) {
                return new SimpleStringProperty(param.getValue().getPatronymic());
            }
        });
        historyTable.getColumns().add(patronymicColumn);

        TableColumn<ReportHistory, String> nameReportColumn = new TableColumn<>("Название справки");
        nameReportColumn.setPrefWidth(100);
        nameReportColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReportHistory, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ReportHistory, String> param) {
                return new SimpleStringProperty(param.getValue().getReportName().getName());
            }
        });
        historyTable.getColumns().add(nameReportColumn);


        TableColumn dateColumn = new TableColumn("Дата");
        dateColumn.setPrefWidth(181.0);
        dateColumn.setCellValueFactory(
                new PropertyValueFactory<ReportHistory, String>("date"));

        historyTable.getColumns().add(dateColumn);

    }

    @FXML
    public void onRefresh() {
        historyTable.getItems().clear();
        initData();
    }

}
