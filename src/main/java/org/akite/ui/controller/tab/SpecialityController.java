package org.akite.ui.controller.tab;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.akite.domain.Speciality;
import org.akite.service.SpecialityService;
import org.akite.ui.dialog.DialogWindow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Admin on 18.03.2016.
 */
@Component
public class SpecialityController {

    @FXML
    TableView<Speciality> specialityTable;
    @Autowired
    SpecialityService specialityService;

    @FXML
    public void initialize() {
        initData();
        initElement();
        initMenu();
    }

    private void initData() {
        specialityTable.setItems(FXCollections.observableArrayList(specialityService.getAll()));
    }

    private void initElement() {
        TableColumn nameColumn = new TableColumn("Специальность");
        nameColumn.setPrefWidth(181.0);
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<Speciality, String>("name"));

        specialityTable.getColumns().add(nameColumn);
    }


    private DialogWindow createAddDialog(String value, String titleWindow, TextField textField) {
        DialogWindow dialogWindow = new DialogWindow();
        dialogWindow.setTitle(titleWindow);
        textField.setText(value);
        textField.setPromptText("Специальность");
        dialogWindow.getPanel().getChildren().addAll(textField, dialogWindow.getButtons());
        return dialogWindow;
    }

    @FXML
    public void onAdd() {

        TextField nameTextField = new TextField();
        DialogWindow window = createAddDialog("", "Добавить", nameTextField);

        window.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Speciality speciality = new Speciality();
                speciality.setName(nameTextField.getText());
                specialityTable.getItems().add(speciality);
                specialityService.add(speciality);
                window.close();
            }
        });
        window.show();
    }

    @FXML
    public void onEdit() {
        Speciality speciality =  specialityTable.getSelectionModel().getSelectedItem();

        if (speciality != null) {
            TextField nameTextField = new TextField();
            DialogWindow window = createAddDialog("", "Редактировать", nameTextField);

            nameTextField.setText(speciality.getName());

            window.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    speciality.setName(nameTextField.getText());
                    specialityTable.getItems().set(specialityTable.getSelectionModel().getSelectedIndex(), speciality);
                    specialityService.update(speciality);
                    window.close();
                }
            });
            window.show();
        }
    }

    @FXML
    public void onDelete() {
        Speciality speciality =  specialityTable.getSelectionModel().getSelectedItem();
        if(speciality != null) {
            DialogWindow dialogWindow = new DialogWindow();
            dialogWindow.setTitle("Удаление");
            dialogWindow.getPanel().getChildren().addAll(new Label("Удалить ?"), dialogWindow.getButtons());
            dialogWindow.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    specialityService.delete(speciality);
                    specialityTable.getItems().remove(speciality);
                    dialogWindow.close();
                }
            });

            dialogWindow.show();
        }
    }

    private void initMenu() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem add = new MenuItem("Добавить");
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onAdd();
            }
        });
        contextMenu.getItems().add(add);

        MenuItem edit = new MenuItem("Редактировать");
        edit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onEdit();
            }
        });
        contextMenu.getItems().add(edit);

        MenuItem delete = new MenuItem("Удалить");
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onDelete();
            }
        });
        contextMenu.getItems().add(delete);

        specialityTable.setContextMenu(contextMenu);
    }
}
