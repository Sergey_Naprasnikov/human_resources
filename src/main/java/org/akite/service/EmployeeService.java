package org.akite.service;

import org.akite.domain.Employee;
import org.akite.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by Admin on 18.03.2016.
 */
@Service
public class EmployeeService extends CommonService<EmployeeRepository, Employee> {
    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        setRepository(employeeRepository);
    }
}
