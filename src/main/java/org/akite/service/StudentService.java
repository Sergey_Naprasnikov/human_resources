package org.akite.service;

import org.akite.domain.Student;
import org.akite.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Admin on 29.01.2016.
 */
@Service
public class StudentService extends CommonService<StudentRepository, Student> {
    @Autowired
    public StudentService(StudentRepository studentRepository) {
        setRepository(studentRepository);
    }

}
