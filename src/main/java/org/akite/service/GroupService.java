package org.akite.service;

import org.akite.domain.Group;
import org.akite.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Admin on 07.10.2015.
 */
@Service
public class GroupService extends CommonService<GroupRepository, Group> {
    @Autowired
    public GroupService(GroupRepository groupRepository) {
        setRepository(groupRepository);
    }
}
