package org.akite.service;

import org.akite.domain.Student;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.*;
import java.util.List;

/**
 * Created by Admin on 10.10.2015.
 */
@Service
public class ImportService {

    @Autowired
    SimpleJdbcInsert importStudents;


    private long timestamp =0;

    List<Map<String, Object>> inserts = new ArrayList<>();

    private List<String> dataXLS = new ArrayList<>();

    public List<Student> importStudentXLS (String path, String name) throws IOException, SAXException, OpenXML4JException, SQLException {
        parseFile(path);
       List<Student> students = new ArrayList<>();
        for(String element : dataXLS) {
            int i =0 ;
        }
        return students;
    }

//    private List<String> importDonorsXLS(String path, String name) {
//        try {
//            parseFile(path);
//            inserts.clear();
//            for(String element : dataXLS) {
//                Map<String, Object> map = new HashMap<>();
//                map.put("link",element);
//                map.put("client_id",currentClient.getId());
//                map.put("add_date", new Date());
//                map.put("version", currentClient.getVersion());
//                map.put("source",name);
//                inserts.add(map);
//                updateDB(importDonors);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return dataXLS;
//    }
//
//
//
//
//    public List<String> importDonors(String path, String name) throws IOException {
//        if(name.indexOf(".xls") != -1)
//            return importDonorsXLS(path, name);
//
//    }



    private void updateDB(SimpleJdbcInsert simpleJdbcInsert) {
        Map[] data = new Map[inserts.size()];
        for(int i=0; i<inserts.size(); i++) {
            data[i] = inserts.get(i);
        }
        simpleJdbcInsert.executeBatch(data);
        inserts.clear();
    }

    public List<String> parse(String urlFile) throws IOException {
        String line = "";
        BufferedReader bufferedReader = new BufferedReader(new FileReader(urlFile));
        List<String> elements = new ArrayList<>();

        while ((line = bufferedReader.readLine()) != null) {
            elements.add(line);
        }
        return elements;
    }



    public void parseFile(String path) throws IOException, SAXException, SQLException, OpenXML4JException {

        OPCPackage pkg = OPCPackage.open(path);
        XSSFReader reader = new XSSFReader(pkg);

        StylesTable styles = reader.getStylesTable();
        SharedStringsTable sst = reader.getSharedStringsTable();

        XMLReader parser = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
        timestamp = System.currentTimeMillis() / 1000L;
        SheetHandler handler = new SheetHandler(sst, styles, timestamp);
        parser.setContentHandler(handler);

        InputStream sheet2 = reader.getSheet("rId1");
        InputSource sheetSource = new InputSource(sheet2);
        parser.parse(sheetSource);
        sheet2.close();
        System.out.println(handler.rowCount);
    }

    enum xssfDataType {
        BOOL,
        ERROR,
        FORMULA,
        INLINESTR,
        SSTINDEX,
        NUMBER,
    }

    private class SheetHandler extends DefaultHandler {
        private long timestamp;
        private SharedStringsTable sst;
        private String lastContents;
        private boolean isImport = false;
        private boolean nextIsString;
        private boolean sheetData = false;
        private boolean isRow = false;
        int rowCount = 0;
        private boolean isColumn = false;
        int columnCounter = 0;
        private boolean isCell = false;
        private String currentCell = "";
        private BidiMap synonyms = new DualHashBidiMap();
        private Map<Integer, String> indexes = new HashMap<>();
        private Map<String, Object> insertParametrs = new HashMap<>();
        private StylesTable stylesTable;
        private xssfDataType nextDataType;
        private short formatIndex;
        private String formatString;
        private final DataFormatter formatter;

        private SheetHandler(SharedStringsTable sst, StylesTable stylesTable, long timestamp) {
            this.sst = sst;
            this.timestamp = timestamp;
            this.stylesTable = stylesTable;
            this.formatter = new DataFormatter();
            this.nextDataType = xssfDataType.NUMBER;
            //   for (HashMap<String, String> row : synonyms) {
            //        this.synonyms.put(row.get("0"), row.get("1"));
            //    }
            inserts.clear();
            dataXLS.clear();
        }

        public void startElement(String uri, String localName, String name,
                                 Attributes attributes) throws SAXException {
            rowCount += name.equals("row") && !isRow ? 1 : 0;
            columnCounter += name.equals("c") && !isColumn ? 1 : 0;
            isColumn = name.equals("c") ? !isColumn : isColumn;
            isRow = name.equals("row") ? !isRow : isRow;
            sheetData = name.equals("sheetData") ? !sheetData : sheetData;
            if (name.equals("c")) {
                nextDataType = xssfDataType.NUMBER;
                currentCell = attributes.getValue("r");
                String cellType = attributes.getValue("t");
                String cellStyleStr = attributes.getValue("s");
                if ("b".equals(cellType))
                    nextDataType = xssfDataType.BOOL;
                else if ("e".equals(cellType))
                    nextDataType = xssfDataType.ERROR;
                else if ("inlineStr".equals(cellType))
                    nextDataType = xssfDataType.INLINESTR;
                else if ("s".equals(cellType))
                    nextDataType = xssfDataType.SSTINDEX;
                else if ("str".equals(cellType))
                    nextDataType = xssfDataType.FORMULA;
                else if (cellStyleStr != null) {
                    // It's a number, but almost certainly one
                    //  with a special style or format
                    int styleIndex = Integer.parseInt(cellStyleStr);
                    XSSFCellStyle style = stylesTable.getStyleAt(styleIndex);
                    this.formatIndex = style.getDataFormat();
                    this.formatString = style.getDataFormatString();
                    if (this.formatString == null) {
                        this.formatString = BuiltinFormats.getBuiltinFormat(this.formatIndex);
                    }

                }
            }
            lastContents = "";
        }

        public void endElement(String uri, String localName, String name)
                throws SAXException {
            String thisStr = null;
            if (name.equals("v")) {
                switch (nextDataType) {

                    case BOOL:
                        char first = lastContents.charAt(0);
                        thisStr = first == '0' ? "FALSE" : "TRUE";
                        dataXLS.add(thisStr);
                        break;

                    case ERROR:
                        thisStr = "\"ERROR:" + lastContents.toString() + '"';
                        dataXLS.add(thisStr);
                        break;

                    case FORMULA:
                        // A formula could result in a string value,
                        // so always add double-quote characters.
                        thisStr = '"' + lastContents.toString() + '"';
                        dataXLS.add(thisStr);
                        break;

                    case INLINESTR:
                        // TODO: have seen an example of this, so it's untested.
                        XSSFRichTextString rtsi = new XSSFRichTextString(lastContents.toString());
                        thisStr = '"' + rtsi.toString() + '"';
                        dataXLS.add(thisStr);
                        break;

                    case SSTINDEX:
                        int idx = Integer.parseInt(lastContents);
                        thisStr = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
                        nextIsString = false;
                        dataXLS.add(thisStr);
                        break;
                    case NUMBER:
                        String n = lastContents.toString();
                        if (this.formatString != null) {
                            if (DateUtil.isADateFormat(formatIndex, formatString)) {
                                Date d = DateUtil.getJavaDate(Double.parseDouble(n));
                                thisStr = DateFormatUtils.format(d, "dd.MM.yyyy");
                            } else {
                                thisStr = NumberUtils.createBigDecimal(n).scale() == 0 ? n : NumberUtils.createBigDecimal(n).setScale(2, RoundingMode.CEILING).toPlainString();
                            }
                        } else {
                            thisStr = n;
                        }
                        dataXLS.add(thisStr);
                        break;

                    default:
                        thisStr = "(TODO: Unexpected type: " + nextDataType + ")";
                        break;
                }
                if (synonyms.containsValue(thisStr) && !isImport) {
                    indexes.put(columnCounter, (String) synonyms.getKey(thisStr));
                } else if (isImport && indexes.containsKey(columnCounter)) {
                    insertParametrs.put(indexes.get(columnCounter), thisStr);
                }
            }
            if (name.equals("row") && isImport) {
                insertParametrs.put("time_stamp", timestamp);
                inserts.add(new HashMap<>(insertParametrs));
                insertParametrs.clear();
            }
            if (name.equals("row")) {
                columnCounter = 0;
                isImport = indexes.size() > 0;
            } else if (name.equals("sheetData")) {
                isImport = false;
            }
            isColumn = name.equals("c") ? !isColumn : isColumn;
            isRow = name.equals("row") ? !isRow : isRow;
            sheetData = name.equals("sheetData") ? !sheetData : sheetData;
        }

        public void characters(char[] ch, int start, int length)
                throws SAXException {
            lastContents += new String(ch, start, length);
        }
    }

}
