package org.akite.service;

import org.akite.domain.Speciality;
import org.akite.repository.SpecialityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Admin on 18.03.2016.
 */
@Service
public class SpecialityService extends CommonService<SpecialityRepository, Speciality> {
    @Autowired
    public SpecialityService(SpecialityRepository specialityRepository) {
        setRepository(specialityRepository);
    }
}
