package org.akite.service;

import org.akite.domain.ReportHistory;
import org.akite.repository.ReportHistoryStudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Admin on 09.04.2016.
 */
@Service
public class ReportHistoryService extends CommonService<ReportHistoryStudentRepository, ReportHistory> {
    @Autowired
    public ReportHistoryService(ReportHistoryStudentRepository reportHistoryStudentRepository) {
        setRepository(reportHistoryStudentRepository);
    }
}
