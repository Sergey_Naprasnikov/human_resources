package org.akite.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import org.akite.domain.*;
import org.akite.repository.ReportNameRepository;
import org.akite.ui.controller.tab.HistoryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 08.04.2016.
 */
@Service
public class ExportService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ReportHistoryService reportHistoryService;
    @Autowired
    ReportNameRepository reportNameRepository;

    @Autowired
    HistoryController historyController;

    public void exportReportEcruitmentOffice(Student student, String path) {

        Map<String, Object> parametrs = new HashMap<>();
        parametrs.put("student", student);
        try{
           // InputStream reportStream = getClass().getResourceAsStream("/reports/st.jasper");

            JasperPrint jasperPrint = JasperFillManager.fillReport(JasperCompileManager
                    .compileReport(getClass().getResourceAsStream("/reports/ecruitment_office.jrxml")), parametrs,new JREmptyDataSource() /*"reports/protocol.jasper"*/
                 /*   parametrs, jdbcTemplate.getDataSource().getConnection()*/);
            //   jdbcTemplate.getDataSource().getConnection().close();

            JRDocxExporter exporter = new JRDocxExporter();

            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, path);
            exporter.exportReport();

            ReportHistory reportHistory = new ReportHistory();
            reportHistory.setName(student.getName());
            reportHistory.setSurname(student.getSurname());
            reportHistory.setPatronymic(student.getPatronymic());
            reportHistory.setTypeUser(student.getTypeUser());

            addHistoryStudentReport(reportHistory, ReportType.ECRUITMENT_OFFICE);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void exportEmployee(Employee employee, String path) {

        Map<String, Object> parametrs = new HashMap<>();
        parametrs.put("employee", employee);
        try{
            // InputStream reportStream = getClass().getResourceAsStream("/reports/st.jasper");

            JasperPrint jasperPrint = JasperFillManager.fillReport(JasperCompileManager
                            .compileReport(getClass().getResourceAsStream("/reports/employee_report.jrxml")), parametrs,new JREmptyDataSource() /*"reports/protocol.jasper"*/
                 /*   parametrs, jdbcTemplate.getDataSource().getConnection()*/);
            //   jdbcTemplate.getDataSource().getConnection().close();

            JRDocxExporter exporter = new JRDocxExporter();

            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, path);
            exporter.exportReport();

//            ReportHistory reportHistory = new ReportHistory();
//            reportHistory.setName(student.getName());
//            reportHistory.setSurname(student.getSurname());
//            reportHistory.setPatronymic(student.getPatronymic());
//            reportHistory.setTypeUser(student.getTypeUser());

       //     addHistoryStudentReport(reportHistory, ReportType.ECRUITMENT_OFFICE);
        }catch(Exception e){
            e.printStackTrace();
        }

    }


    private void addHistoryStudentReport(ReportHistory reportHistory, ReportType reportType) {
        ReportName reportName = reportNameRepository.findByReportType(reportType);
        reportHistory.setDate(new Date());
        reportHistory.setReportName(reportName);
        reportHistoryService.add(reportHistory);
        historyController.onRefresh();
    }

}
