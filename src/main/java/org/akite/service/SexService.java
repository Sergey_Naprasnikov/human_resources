package org.akite.service;

import org.akite.domain.Sex;
import org.akite.repository.SexRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Admin on 18.03.2016.
 */
@Service
public class SexService extends CommonService<SexRepository, Sex> {

    @Autowired
    public SexService(SexRepository sexRepository) {
        setRepository(sexRepository);
    }

}
