package org.akite.test;

import org.akite.Application;
import org.akite.config.Config;
import org.akite.domain.Employee;
import org.akite.domain.Student;
import org.akite.service.ExportService;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

/**
 * Created by Admin on 08.04.2016.
 */

public class ExportTest {

 //   @Autowired
    ExportService exportService = new ExportService();

    @Test
    public void should_exportingEmployee() {
        Employee employee = new Employee();
        employee.setName("Test user");
        employee.setSurname("111111");
        exportService.exportEmployee(employee, "D:\\1.docx");
    }

    @Test
    public void should_exportReportEcruitmentOffice() {
        Student billy = new Student();
        billy.setName("Billy");
        billy.setSurname("Ouhsen");
        billy.setPatronymic("jjjjj");
        billy.setYearBirth(new Date());
        billy.setYearRevenue(new Date());


        exportService.exportReportEcruitmentOffice(billy, "D:\\1.docx");
    }

}
